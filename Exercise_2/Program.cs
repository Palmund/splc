﻿using System;

namespace Exercise_2
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			{
				Time t1 = new Time (10, 05);
				Console.WriteLine (t1.ToString());

				Time t2 = new Time (9, 30);
				Console.WriteLine (t2 + new Time (1, 15));
				Console.WriteLine (t2 + new Time (1, 15));
			}
			{ // Exercise 1.4
				Time t1 = new Time (9, 30);
				Time t2 = 120; // Two hours
				int m1 = (int)t1;
				Console.WriteLine ($"t1={t1} and t2={t2} and m1={m1}");
				Time t3 = t1 + 45;

				// The addition in t3 is legal because 45 is implicitly converted to an instance of Time.
				// The value t3 = t1.minutes + 45 = (60*9+30) + 45 = 615
				// 615 translates into 10:15
			}

			Console.WriteLine ("Exercise 2.1");
			{ // Exercise 2.1
				Time t1 = new Time (9, 30);
				Time t2 = t1;
				t1.minutes = 100;
				Console.WriteLine ($"t1={t1} and t2={t2}");
			}

			Console.WriteLine ("Exercise 2.2");
			{ // Exercise 2.2
				Time t1 = new Time (9, 30);
				Time t2 = new Time (9, 30);
				Time t3 = new Time (9, 30);
				TimeMethods.AddOneHour (t1);
				TimeMethods.AddOneHourByRef (ref t2);
				t3.AddOneHourInstance ();
				Console.WriteLine ($"t1={t1} and t2={t2} and t3={t3}");
			}
		}
	}
}
