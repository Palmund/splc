﻿using System;

namespace Exercise_2
{
	public class TimeMethods
	{
		public static void AddOneHour (Time t) => t.minutes += 60;

		public static void AddOneHourByRef (ref Time t) => t.minutes += 60;
	}
}

