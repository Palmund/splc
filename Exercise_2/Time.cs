﻿using System;

namespace Exercise_2
{
	public struct Time
	{
		public int Hour => minutes / 60;
		public int Minute => minutes - Hour * 60;

		internal int minutes;

		public Time(int hh, int mm)
			: this (60 * hh + mm) {
		}

		public Time(int minutes) 
			: this() {
			this.minutes = minutes;
		}

		public void AddOneHourInstance () => minutes += 60;

		public override string ToString () {
			return string.Format ("{0}:{1}", Hour.ToString ("D"), Minute.ToString ("D2"));
		}

		public static Time operator +(Time t1, Time t2) => new Time(t1.minutes + t2.minutes);

		public static Time operator -(Time t1, Time t2) => new Time(t1.minutes - t2.minutes);

		public static implicit operator Time(int minutes) => new Time(minutes);

		public static explicit operator int(Time t) => t.minutes;
	}

	class Test
	{
		private static Test _instance = new Test();
		private static readonly int X = 2;

		public Test() {
			Console.WriteLine ($"x={X}");
		}
	}
}

