﻿using System;
using Ex04;
using static Exercises_3.TestDelegate;

namespace Exercises_3
{
	public class Tests
	{
		static int BookCompare (Book a, Book b) => a.title.CompareTo(b.title);

		public static void Main () {
			Console.WriteLine ("Exercise 1.1");
			{ // Exercise 1.1
				IntAction a = PrintInt;
				a (42);
				Perform (PrintInt, 1, 2, 3, 4);
				Perform (Console.WriteLine, 1, 2, 3, 4); // Using Console.WriteLine instead of PrintInt
			}

			Console.WriteLine ("\nExercise 2.1");
			{ // Exercise 2.1
				Book[] books = new Book[] {
					new Book ("Peter Sestoft", "C# Precisely", 2012),
					new Book ("Peter Sestoft", "Programming Language Concepts", 2012),
					new Book ("Eva Tardos and Jon Kleinberg", "Algorithm Design", 2005),
					new Book ("Goetz et al", "Java Concurrency in Practice", 2006),
					new Book ("Armstrong, Virding, Williams", "Concurrent Programming in Erlang", 0000)
				};
				foreach (Book book in books) {
					Console.WriteLine (book);
				}
				Console.WriteLine ("------");
				GenericMethods.Quicksort (books, BookCompare);
				foreach (Book book in books) {
					Console.WriteLine (book);
				}
			}

			Console.WriteLine ("\nExercise 2.2");
			{ // Exercise 2.2
				DComparer<Book> cmp = ByYear;
				DComparer<Book> cmpLambda = (Book a, Book b) => (a.year == b.year) 
					? a.title.CompareTo (b.title)
					: a.year.CompareTo (b.year);
				Book[] books = new Book[] {
					new Book ("Peter Sestoft", "Programming Language Concepts", 2012),
					new Book ("Peter Sestoft", "C# Precisely", 2012),
					new Book ("Eva Tardos and Jon Kleinberg", "Algorithm Design", 2005),
					new Book ("Goetz et al", "Java Concurrency in Practice", 2006),
					new Book ("Armstrong, Virding, Williams", "Concurrent Programming in Erlang", 0000)
				};
				foreach (Book book in books) {
					Console.WriteLine (book);
				}
				Console.WriteLine ("------");
				GenericMethods.Quicksort (books, cmp);
				foreach (Book book in books) {
					Console.WriteLine (book);
				}
			}

			Console.WriteLine ("\nExercise 2.3");
			{ // Exercise 2.3
				Book[] books = new Book[] {
					new Book ("Peter Sestoft", "Programming Language Concepts", 2012),
					new Book ("Peter Sestoft", "C# Precisely", 2012),
					new Book ("Eva Tardos and Jon Kleinberg", "Algorithm Design", 2005),
					new Book ("Goetz et al", "Java Concurrency in Practice", 2006),
					new Book ("Armstrong, Virding, Williams", "Concurrent Programming in Erlang", 0000)
				};
				foreach (Book book in books) {
					Console.WriteLine (book);
				}
				Console.WriteLine ("------");
				Book[] fBooks = GenericMethods.Filter (books, (b) => b.year < 2004);
				foreach (Book book in fBooks) {
					Console.WriteLine (book);
				}
			}

			Console.WriteLine ("\nExercise 2.4");
			{ // Exercise 2.4
				Book[] books = new Book[] {
					new Book ("Peter Sestoft", "Programming Language Concepts", 2012),
					new Book ("Peter Sestoft", "C# Precisely", 2012),
					new Book ("Eva Tardos and Jon Kleinberg", "Algorithm Design", 2005),
					new Book ("Goetz et al", "Java Concurrency in Practice", 2006),
					new Book ("Armstrong, Virding, Williams", "Concurrent Programming in Erlang", 0000)
				};
				string[] titles = GenericMethods.Map (books, (b) => b.title);
				foreach (string title in titles) {
					Console.WriteLine (title);
				}
			}
		}

		private static int ByYear(Book b1, Book b2) {
			if (b1.year == b2.year) {
				return b1.title.CompareTo (b2.title);
			}
			else {
				return b1.year.CompareTo (b2.year);
			}
		}
	}
}

