﻿using System;

namespace Exercises_3
{
	public class TestDelegate
	{
		public delegate void IntAction(int i);

		public static void PrintInt(int i) => Console.WriteLine(i);

		public static void Perform(IntAction act, params int[] arr) {
			foreach (int i in arr) {
				act (i);
			}
		}
	}
}

