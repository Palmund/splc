﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercises_4
{
	class Handin
	{
		#region Exercise 1.1
		public static bool Forall (int[] xs, Func<int,bool> p)
		{
			foreach (int x in xs) {
				if (!p (x)) {
					return false;
				}
			}
			return true;
		}
			
		public static bool Exists (int[] xs, Func<int,bool> p)
		{
			foreach (int x in xs) {
				if (p (x)) {
					return true;
				}
			}
			return false;
		}
		#endregion

		#region Exercise 1.2
		public static bool Forall<T> (T[] xs, Func<T,bool> p)
		{
			foreach (T x in xs) {
				if (!p (x)) {
					return false;
				}
			}
			return true;
		}

		public static bool Exists<T> (T[] xs, Func<T,bool> p)
		{
			foreach (T x in xs) {
				if (p (x)) {
					return true;
				}
			}
			return false;
		}
		#endregion

		#region Exercise 1.3
		public static List<int> Merge(List<int> l1, List<int> l2) {
			var size = l1.Count + l2.Count;
			var merged = new List<int> (size);

			int i1 = 0, i2 = 0;
			for (int i = 0; i < size; i++) {
				int h1 = i1 == l1.Count ? int.MaxValue : l1 [i1];
				int h2 = i2 == l2.Count ? int.MaxValue : l2 [i2];
				if (h1 < h2) {
					merged.Add (h1);
					i1++;
				} 
				else if (h2 < h1) {
					merged.Add (h2);
					i2++;
				}
				else {
					merged.Add (h1);
					i += 1;
					merged.Add (h2);
					i1++;
					i2++;
				}
			}
			return merged;
		}
		#endregion

		#region Exercise 1.4
		public static List<T> Merge<T>(List<T> l1, List<T> l2) where T : IComparable<T> {
			var size = l1.Count + l2.Count;
			var merged = new List<T> (size);

			int i1 = 0, i2 = 0;
			for (int i = 0; i < size; i++) {
				if (i1 == l1.Count && i2 != l2.Count) {
					merged.Add (l2 [i2++]);
				} 
				else if (i2 == l2.Count && i1 != l1.Count) {
					merged.Add (l1 [i1++]);
					continue;
				}

				T h1 = l1 [i1];
				T h2 = l2 [i2];
				int cmp = h1.CompareTo (h2);
				if (cmp < 0) {
					merged.Add (h1);
					i1++;
				}
				else if (cmp > 0) {
					merged.Add (h2);
					i2++;
				}
				else {
					merged.Add (h1);
					i += 1;
					merged.Add (h2);
					i1++;
					i2++;
				}
			}
			return merged;
		}
		#endregion

		#region Exercise 1.5
//		Exercise 1.5 (optional) Construct a generic method Compose that takes two functions and composes them. For
//		example, if f has type Func<int, int> then Compose(f,f) should also be of type Func<int, int>
//			and should actually be the same as Twice(f) known from the slides. But we should be able to compose any pair
//		of functions if only the return type of the first matches the input type of the second.
//			Suppose now, U is a subclass of T and f has type Func<T,T> and g has type Func<U,U>. Which of the
//			expressions Compose(g,f) and Compose(f,g) is welltyped? Justify your answer.
		public static Func<A,C> Compose<A,B,C>( Func<A,B> f, Func<B,C> g) => a => g(f(a));
		#endregion
		
		#region Exercise 2.1

		#endregion
	}

	class Tests
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("\nExercise 1.1");
			{ // Exercise 1.1
				var arr = new int[] { 1, 2, 3, 4 };
				var allEvens = new int[] { 2, 4, 6 };

				Console.WriteLine ("Forall");
				Console.WriteLine ("[1,2,3,4] even: " + Expect(false, Handin.Forall (arr, i => i % 2 == 0)));
				Console.WriteLine ("[2,4,6] even: " + Expect(true, Handin.Forall (allEvens, i => i % 2 == 0)));

				Console.WriteLine ("Exists");
				Console.WriteLine ("[1,2,3,4] find 2: " + Expect(true, Handin.Exists (arr, i => i == 2)));
				Console.WriteLine ("[2,4,6] find 5: " + Expect(false, Handin.Exists (allEvens, i => i == 5)));	
			}

			Console.WriteLine ("\nExercise 1.2");
			{ // Exercise 1.2
				var arr = new int[][] { 
					new int[] { 1, 2, 3, 4 },
					new int[2]
				};
				var arr1 = new int[][] { 
					new int[] { 1, 2, 3 },
					new int[] { 3, 4, 5 }
				};
				var naturals = new int[] { 1, 2, 3, 4 };
				var allEvens = new int[] { 2, 4, 6 };

				Console.WriteLine ("Forall generic");
				Console.WriteLine ("[1,2,3,4] even: " + Expect(false, Handin.Forall<int> (naturals, i => i % 2 == 0)));
				Console.WriteLine ("[2,4,6] even: " + Expect(true, Handin.Forall<int> (allEvens, i => i % 2 == 0)));

				Console.WriteLine ("Exists generic");
				Console.WriteLine ("[1,2,3,4] find 2: " + Expect(true, Handin.Exists<int> (naturals, i => i == 2)));
				Console.WriteLine ("[2,4,6] find 5: " + Expect(false, Handin.Exists<int> (allEvens, i => i == 5)));	

				Console.WriteLine ("Forall length");
				Console.WriteLine ("[[1,2,3,4],[]] len > 2: " + Expect(false, Handin.Forall<int[]> (arr, i => i.Length > 2)));
				Console.WriteLine ("[[1,2,3,4],[]] len > 2: " + Expect(true, Handin.Forall<int[]> (arr1, i => i.Length > 2)));
			}

			Console.WriteLine ("\nExercise 1.3");
			{ // Exercise 1.3
				var l1 = new List<int> { 0, 2, 4 };
				var l2 = new List<int> { 1, 3 };
				var l3 = Handin.Merge (l1, l2);
				Console.WriteLine ("[0,2,4] & [1,3] Merge: " + Expect (true, l3.SequenceEqual (new List<int> { 0, 1, 2, 3, 4 })));
			}

			Console.WriteLine ("\nExercise 1.4");
			{ // Exercise 1.4
				var l1 = new List<int> { 0, 2, 4 };
				var l2 = new List<int> { 1, 3 };
				var l3 = Handin.Merge<int> (l1, l2);
				Console.WriteLine ("[0,2,4] & [1,3] Merge generic: " + Expect (true, l3.SequenceEqual (new List<int> { 0, 1, 2, 3, 4 })));
			}

			Console.WriteLine ("\nExercise 1.5");
			{ // Exercise 1.4
				Func<int,int> DoubleIt = x => x * 2;
				Func<int,int> DivideIt = x => x / 2;
				Func<int,int> composed = Handin.Compose<int,int,int> (DoubleIt, DivideIt);
				Console.WriteLine ("DoubleIt compose DivideIt: " + Expect (true, composed(2) == 2));

				Func<Person, Person> f = (Person p) => p;
				Func<Student, Student> g = (Student s) => s;

//				Func<Person, Student> composed1 = Handin.Compose<Person,Student,Person> (f, g);
				Console.WriteLine ("Person->Person compose Student->Student: " + Expect (true, composed(2) == 2));
			}

			Console.WriteLine ("\nExercise 2.1");
			{ // Exercise 2.1
				Console.WriteLine ("GreaterCount: " + Expect (1, Further.GreaterCount (Further.temperatures, 25)));
			}

			Console.WriteLine ("\nExercise 2.2");
			{ // Exercise 2.2
				double[] temps = new double[] {
					10.0, 20.0, 30.0
				};
				Console.WriteLine ("GreaterCount generic: " + Expect (1, Further.GreaterCount (temps, 25)));
				Console.WriteLine ("GreaterCount generic: " + Expect (1, Further.GreaterCount (Further.temperatures as IEnumerable<double>, 25)));
			}

			Console.WriteLine ("\nExercise 2.3");
			{ // Exercise 2.3
				double[] temps = new double[] {
					10.0, 20.0, 30.0
				};
				Console.WriteLine ("GreaterCount generic: " + Expect (1, Further.GreaterCount<double> (temps, 25)));
				Console.WriteLine ("GreaterCount generic: " + Expect (1, Further.GreaterCount<double> (Further.temperatures as IEnumerable<double>, 25)));
			}

			Console.WriteLine ("\nExercise 2.4");
			{ // Exercise 2.4
				Student s1 = new Student() { CPR = 0 };
				Student s2 = new Student() { CPR = 2 };
				Student s3 = new Student() { CPR = 3 };
				IEnumerable<Student> xs = new Student[] {s1, s2, s3};
				IEnumerable<Person> ys = xs;

				Student x = new Student () { CPR = -1 };
				int result = Further.GreaterCount(xs, x);
				Console.WriteLine(result);
				Console.WriteLine ("Student GreaterCount: " + Expect (3, result));
			}
		}

		private static string Expect (bool expected, bool actual)
		{
			if (expected == actual) {
				return "Pass";
			} else {
				return "FAIL";
			}
		}

		private static string Expect (int expected, int actual)
		{
			if (expected == actual) {
				return "Pass";
			} else {
				return "FAIL";
			}
		}
	}

	internal class Person : IComparable<Person> {
		public double CPR { get; set;}

		#region IComparable implementation

		public int CompareTo (Person that)
		{
			return this.CPR.CompareTo (that.CPR);
		}

		#endregion
}
	internal class Student : Person {}

	class Further {
		#region Exercise 2.1 & 2.2
		public static List<double> temperatures = new List<double> {
			10.0, 20.0, 30.0
		};

		public static int GreaterCount(List<double> list, double min) {
			int c = 0;
			foreach (double t in list) {
				if (t >= min) {
					c++;
				}
			}
			return c;
		}

		public static int GreaterCount(IEnumerable<double> eble, double min) {
			int c = 0;
			foreach (double t in eble) {
				if (t >= min) {
					c++;
				}
			}
			return c;
		}
		#endregion

		#region Exercise 2.3
		public static int GreaterCount<T>(IEnumerable<T> eble, T x) where T : IComparable<T> {
			int c = 0;
			foreach (T t in eble) {
				if (t.CompareTo(x) >= 0) {
					c++;
				}
			}
			return c;
		}
		#endregion
	}
}
